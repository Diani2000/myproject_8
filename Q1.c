#include <stdio.h>

struct details{
	char firstName[10];
	char subject[10];
	int mark;
	
};

int main(){
	int n;
	printf("Input Number of Student : ");
	scanf("%d",&n);
	struct details d[6];

	for(int i=0; i<n; i++){
		printf("Input First Name : ");
		scanf("%s", &d[i].firstName);
		printf("Input Subject Name : ");
		scanf("%s", &d[i].subject);
		printf("Input Marks : ");
		scanf("%d", &d[i].mark);	
	}
	
	for(int j=0; j<n; j++){
		printf("\n %s     %s      %d \n ",d[j].firstName, d[j].subject, d[j].mark);
	}
}
